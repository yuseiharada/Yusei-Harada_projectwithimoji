color a,b,c;
float x;
float y;
float z;
float easing = 0.1; //the speed of the movement is faster. 

void setup() {
  size(640, 360); 
  noStroke();  

}

void draw() { 
  background(22,44,255); //changed the background color.
  
  float targetX = mouseX;
  float dx = targetX - x;
  x += dx * easing;
  
  float targetY = mouseY;
  float dy = targetY - y;
  y += dy * easing;
  a = color(166, 177, 20);
  fill(255,0,0); triangle(x, y, z, 66, 66, 144);   //changed to triangles and colors 
   fill (255,255,255); triangle(x, y, z, 540, 200, 600);  //changed to triangles and colors 
  fill (0, 255, 0); triangle(x, y, z, 8000 ,80000, 800000);  //changed to triangles and colors 
}