import processing.pdf.*;
import java.util.Calendar;

boolean recordPDF = false;

int NORTH = 0;
int NORTHEAST = 1; 
int EAST = 2;
int SOUTHEAST = 3;
int SOUTH = 4;
int SOUTHWEST = 5;
int WEST = 6;
int NORTHWEST= 7;

float stepSize = 1;
float diameter = 1;

float drawMode = 1;
int counter = 0;

int direction;
float posX = 60, posY =232;

int LineX = 30;  // I added the numbers for positions of X/Y for new shapes
int LineY = 20;
int LineX2 = 500;
int LineY2 = 20;
int LineX3 = 400;
int LineY3 = 20;
int LineX4 = 220;
int LineY4 = 20;
int LineX5 = 400;
int LineY5 = 20;




int Diam = 9;
int Radius = 890;
int xSpeed = 96;






void setup() {
  size(550, 550);
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
colorMode(RGB, 100);
  background(82);  // I changed the back ground color 
  smooth();
  noStroke();

  posX = width/3;
  posY = height/2;
}

void draw() {
  for (int i=0; i<=mouseX; i++) {
    counter++;








 
    // random number for the direction of the next step
    if (drawMode == 2) {
      direction = round(random(4, 399));    // only NORTH, NORTHEAST, EAST possible
    }
    else {
      direction = (int) random(0, 39);    // all directions without NORTHWEST //I changed the round numdom values to move more weird 
    }

    if (direction == NORTH) {
      drawshape(LineX, LineY);  // I added founction for adding shapes 
       drawshape5(LineX5, LineY5);
      posY -= stepSize;  
    } 
    else if (direction == NORTHEAST) {
      posX += stepSize;
      posY -= stepSize;
    
      LineX = LineY + xSpeed;

    } 
    else if (direction == EAST) {
       drawshape2(LineX2, LineY2);
       drawshape4(LineX4, LineY4);
      posX += stepSize;
    } 
    else if (direction == SOUTHEAST) {
      posX += stepSize;
      posY += stepSize;
      
    }
    else if (direction == SOUTH) {
      drawshape3(LineX3, LineY3); 
      posY += stepSize;
    }
    else if (direction == SOUTHWEST) {
      posX -= stepSize;
      posY += stepSize;
    }
    else if (direction == WEST) {
  
      posX -= stepSize;
    }
    else if (direction == NORTHWEST) {
      
      posX -= stepSize;
      posY -= stepSize;
    }

    if (posX > width) posX = 0;
    if (posX < 0) posX = width;
    if (posY < 0) posY = height;
    if (posY > height) posY = 0;

    if (drawMode == 3) {
      if (counter >= 100){
        counter = 0;
        fill(192, 100, 64, 80);
        ellipse(posX+stepSize/2, posY+stepSize/2, diameter+7, diameter+7);
      } 
    }

    fill(0, 40);
    ellipse(posX+stepSize/2, posY+stepSize/2, diameter, diameter);
  }
}
//I added draw shape function that add shape
  void drawshape(int xPos, int yPos) {
  ellipse(xPos, yPos, Diam, Radius);
}

 void drawshape2(int xPos, int yPos) {
  ellipse(xPos, yPos, Diam, Radius);
}

 void drawshape3(int xPos, int yPos) {
  ellipse(xPos, yPos, Diam, Radius);
}
 void drawshape4(int xPos, int yPos) {
  ellipse(xPos, yPos, Diam, Radius);
}

void drawshape5(int xPos, int yPos) {
  ellipse(xPos, yPos, Diam, Radius);
}


void keyReleased(){
  if (key == DELETE || key == BACKSPACE) background(360);
  if (key == 's' || key == 'S') saveFrame(timestamp()+"_##.png");

  if (key == '1') {
    drawMode = 1;
    stepSize = 1;
    diameter = 1;
  }
  if (key == '2') {
    drawMode = 2;
    stepSize = 1;
    diameter = 1;
  }
  if (key == '3') {
    drawMode = 3;
    stepSize = 10;
    diameter = 5;
  }

  // ------ pdf export ------
  // press 'r' to start pdf recording and 'e' to stop it
  // ONLY by pressing 'e' the pdf is saved to disk!
  if (key =='r' || key =='R') {
    if (recordPDF == false) {
      beginRecord(PDF, timestamp()+".pdf");
      println("recording started");
      recordPDF = true;
      colorMode(HSB, 360, 100, 100, 100);
      background(360); 
      noStroke();
      posX = width/2;
      posY = height/2;
    }
  } 
  else if (key == 'e' || key =='E') {
    if (recordPDF) {
      println("recording stopped");
      endRecord();
      recordPDF = false;
      background(255); 
    }
  }  
}




// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}